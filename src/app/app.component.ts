import {Component} from '@angular/core';
import {App, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {LoginPage} from "../pages/login/login";
import {AngularFireAuth} from "angularfire2/auth";
import {TabsPage} from "../pages/tabs/tabs";
import {HomePage} from "../pages/home/home";
import {AuthService} from "../shared/services/auth.service";
import {NavControllerBase} from "ionic-angular/navigation/nav-controller-base";
import {Storage} from "@ionic/storage";
import {TutorialPage} from "../pages/tutorial/tutorial";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  private rootPage: any;

  constructor(private platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private app: App, afAuth: AngularFireAuth, private authService: AuthService, private storage: Storage) {
    this.storage.get("tutorial").then(value => {
      if (value) {
        afAuth.authState.subscribe(user => {
          this.authService.user = user;
          this.rootPage = user ? TabsPage : LoginPage;
        });
      } else {
        this.rootPage = TutorialPage;
      }
    });

    platform.ready().then(() => {
      statusBar.styleDefault();
      statusBar.overlaysWebView(false);
      splashScreen.hide();
      this.handleBackButton();
    });
  }

  handleBackButton() {
    this.platform.registerBackButtonAction(() => {
      let navCtrl: NavControllerBase = this.app.getActiveNav();
      let view = navCtrl.getActive();

      if (navCtrl.canGoBack()) {
        navCtrl.pop();
      } else if (view.instance instanceof HomePage) {
        this.platform.exitApp();
      } else {
        navCtrl.parent.select(0);
      }
    });
  }
}
