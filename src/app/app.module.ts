import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { DrawNewPage } from '../pages/draw-new/draw-new';
import { CollectionsPage } from '../pages/collections/collections';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { NightlightPage } from "../pages/nightlight/nightlight";
import { TabsPage } from "../pages/tabs/tabs";
import {TutorialPage} from "../pages/tutorial/tutorial";
import {CollectionDetailsPage} from "../pages/collection-details/collection-details";

import { environment } from "../environments/environment";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFireModule } from "angularfire2";
import { AngularFireAuthModule } from "angularfire2/auth";
import { AngularFireDatabaseModule } from "angularfire2/database";
import {Network} from "@ionic-native/network";
import {Zeroconf} from "@ionic-native/zeroconf";
import {MqttModule, MqttService} from 'ngx-mqtt';

import {AuthService} from "../shared/services/auth.service";
import {CollectionsPopoverPage} from "../pages/collections/collections-popover";
import {SortPipe} from "../shared/pipes/sort";
import {CollectionDetailsPopoverPage} from "../pages/collection-details/collection-details-popover";
import {DrawDetailsPage} from "../pages/draw-details/draw-details";
import {CollectionsService} from "../shared/services/collections.service";
import {NightlightsService} from "../shared/services/nightlights.service";
import {DrawsService} from "../shared/services/draws.service";
import {IonicStorageModule} from "@ionic/storage";

export const MQTT_SERVICE_OPTIONS = { hostname: 'nightliot.ovh', port: 9001, path: ''};

export function mqttServiceFactory() {
  return new MqttService(MQTT_SERVICE_OPTIONS);
}

@NgModule({
  declarations: [
    MyApp,
    DrawNewPage,
    CollectionDetailsPage,
    CollectionDetailsPopoverPage,
    CollectionsPage,
    CollectionsPopoverPage,
    DrawDetailsPage,
    HomePage,
    LoginPage,
    NightlightPage,
    TabsPage,
    TutorialPage,
    SortPipe
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    MqttModule.forRoot({ provide: MqttService, useFactory: mqttServiceFactory }),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    DrawNewPage,
    CollectionDetailsPage,
    CollectionDetailsPopoverPage,
    CollectionsPage,
    CollectionsPopoverPage,
    DrawDetailsPage,
    HomePage,
    LoginPage,
    NightlightPage,
    TabsPage,
    TutorialPage
  ],
  providers: [
    AuthService,
    CollectionsService,
    DrawsService,
    NightlightsService,
    Network,
    StatusBar,
    SplashScreen,
    Zeroconf,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
