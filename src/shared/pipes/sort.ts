import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
  name: "sort",
  pure: false
})
export class SortPipe implements PipeTransform {
  transform(array: any[], field: string): any[] {
    if (array == null ||  array !instanceof Array) {
      return array;
    }

    let inverted = field.startsWith('-');
    field = inverted ? field.substr(1) : field;

    array.sort((a: any, b: any) => {
      let compareA = a[field];
      let compareB = b[field];

      if (compareA instanceof Array && compareB instanceof Array) {
        compareA = compareA.length;
        compareB = compareB.length;
      }

      if (compareA < compareB) { return inverted ? 1 : -1 }
      else if (compareA > compareB) { return inverted ? -1 : 1 }
      else { return 0 }
    });

    return array;
  }
}
