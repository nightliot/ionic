import {Injectable} from "@angular/core";
import {AngularFireAuth} from "angularfire2/auth";
import {AngularFireDatabase} from "angularfire2/database";
import {User} from "firebase";
import {LoadingController} from "ionic-angular";

@Injectable()
export class AuthService {
  user: User = null;

  constructor(private afAuth: AngularFireAuth, private afDatabase: AngularFireDatabase, private loadingCtrl: LoadingController) {}

  get currentUser(): any {
    return this.user;
  }

  get authState() {
    return this.afAuth.authState;
  }

  login(email: string, password: string, errCallback?: (error: string) => void) {
    let loading = this.loadingCtrl.create({ content: "Loading..." });
    loading.present();

    this.afAuth.auth.signInWithEmailAndPassword(email, password).then(user => {
      this.user = user;
      this.updateUserData();
      loading.dismiss();
    }).catch((error) => {
      loading.dismiss();
      if (errCallback) {
        errCallback(error);
      }
    });
  }

  register(email: string, password: string, errCallback?: (error: string) => void) {
    let loading = this.loadingCtrl.create({ content: "Loading..." });
    loading.present();

    this.afAuth.auth.createUserWithEmailAndPassword(email, password).then(user => {
      this.user = user;
      this.updateUserData();
      loading.dismiss();
    }).catch(error => {
      loading.dismiss();
      if (errCallback) {
        errCallback(error);
      }
    });
  }

  signOut() {
    this.afAuth.auth.signOut();
  }

  updateUserData() {
    let path = `users/${this.user.uid}`;
    let data = { email: this.user.email, name: this.user.displayName};
    this.afDatabase.object(path).update(data);
  }
}
