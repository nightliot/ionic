import {Injectable} from "@angular/core";
import {AngularFireDatabase, AngularFireList} from "angularfire2/database";
import {Collection} from "../models/collection";
import {AuthService} from "./auth.service";
import {AlertController} from "ionic-angular";

@Injectable()
export class CollectionsService {
  public defaults: Collection[] = [];
  public customs: Collection[] = [];
  public customCollectionsRef: AngularFireList<Collection>;
  public defaultCollectionsRef: AngularFireList<Collection>;

  constructor(private afDB: AngularFireDatabase, private authService: AuthService, private alertCtrl: AlertController) {
    this.customCollectionsRef = this.afDB.list<Collection>(`users/${this.authService.user.uid}/collections`);
    this.defaultCollectionsRef = this.afDB.list<Collection>(`defaults/collections`);
    this.getCollections();
  }

  private getCollections() {
    this.customCollectionsRef.snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    }).subscribe(collections => {
      this.customs = collections;
    });

    this.defaultCollectionsRef.snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    }).subscribe(collections => {
      this.defaults = collections;
    });
  }

  deleteSelectedCollections(collections: Collection[], callback?: () => void) {
    let alert = this.alertCtrl.create({
      title: 'Delete collections', message: 'Do you want to delete ' + (collections.length) + ' collection(s)?', cssClass: 'alert-buttons',
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
      }, {
        text: 'Delete',
        cssClass: 'button-delete',
        handler: () => {
          collections = collections.filter((collection) => {
            this.customCollectionsRef.remove(collection.key);
            if (callback) {
              callback();
            }
          });
        }
      }]
    });

    alert.present();
  }

  addCollection() {
    let alert = this.alertCtrl.create({
      title: 'Add collection', message: 'Please enter the name of your new custom collection', cssClass: 'alert-buttons',
      inputs: [{
        name: 'name',
        placeholder: 'Collection name',
        type: 'text'
      }],
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
      }, {
        text: 'Add',
        cssClass: 'button-add',
        handler: (data) => {
          this.customCollectionsRef.push({ name: data.name, date: Date.now(), draws: [] });
        }
      }]
    });

    alert.present();
  }

  rename(collection: Collection) {
    let alert = this.alertCtrl.create({
      title: 'Rename collection', message: 'Please enter the new name of your collection', cssClass: 'alert-buttons',
      inputs: [{
        name: 'name',
        placeholder: 'Collection name',
        type: 'text'
      }],
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
      }, {
        text: 'Rename',
        cssClass: 'button-add',
        handler: (data) => {
          collection.name = data.name;
          this.customCollectionsRef.update(collection.key, collection);
        }
      }]
    });

    alert.present();
  }

  delete(collection: Collection, callback?: () => void) {
    let alert = this.alertCtrl.create({
      title: 'Delete collection', message: 'Do you want to delete ' + (collection.name) + ' collection ?', cssClass: 'alert-buttons',
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
      }, {
        text: 'Delete',
        cssClass: 'button-delete',
        handler: () => {
          this.customCollectionsRef.remove(collection.key);
          if (callback) {
            callback();
          }
        }
      }]
    });

    alert.present();
  }
}
