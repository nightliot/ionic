import {Injectable} from "@angular/core";
import {AngularFireDatabase, AngularFireList} from "angularfire2/database";
import {AuthService} from "./auth.service";
import {Nightlight} from "../models/nightlight";
import {Network} from "@ionic-native/network";
import {AlertController, Platform} from "ionic-angular";
import {Subscription} from "rxjs/Subscription";
import {Zeroconf} from "@ionic-native/zeroconf";
import {MqttService} from "ngx-mqtt";
import {Draw} from "../models/draw";

@Injectable()
export class NightlightsService {
  public nightlights: Nightlight[] = [];
  public nightlightsRef: AngularFireList<Nightlight>;
  public disconnectionSub: Subscription;
  public connectionSub: Subscription;
  public wifi = false;

  constructor(private mqttService: MqttService, private afDB: AngularFireDatabase, private authService: AuthService, private network: Network, private platform: Platform, private zeroConf: Zeroconf, private alertCtrl: AlertController) {
    this.nightlights = [];
    this.nightlightsRef = this.afDB.list<Nightlight>(`users/${this.authService.user.uid}/nightlights`);
  }

  watchNetwork() {
    if (!this.platform.is('cordova')) {
      return;
    }

    if (this.network.type === 'wifi') {
      this.wifiConnectionHandler();
    }

    this.disconnectionSub = this.network.onDisconnect().subscribe(() => this.disconnectionHandler());
    this.connectionSub = this.network.onConnect().subscribe(() => this.connectionHandler());
  }

  public unwatchNetwork() {
    this.disconnectionSub.unsubscribe();
    this.connectionSub.unsubscribe();
  }

  disconnectionHandler() {
    this.wifi = false;
    this.zeroConf.stop();
  }

  connectionHandler() {
    setTimeout(() => {
      if (this.network.type === 'wifi') {
        this.wifiConnectionHandler();
      } else {
        this.disconnectionHandler();
      }
    }, 2700);
  }

  wifiConnectionHandler() {
    this.wifi = true;
    this.scanNightlights();
    setInterval(() => this.scanNightlights(), 3000);
  }

  scanNightlights() {
    this.zeroConf.watch('_nightliot._tcp.', 'local.').subscribe(result => {
      let newNightlight: Nightlight = result.service;
      let discovered = this.nightlights.filter(item => { return item.name == newNightlight.name });
      newNightlight.customName = newNightlight.name;

      if (result.action == 'added') {
        this.getByName(newNightlight.name).snapshotChanges().map(changes => {
          return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
        }).subscribe(values => {
          discovered = this.nightlights.filter(item => { return item.name == newNightlight.name });

          if (values.length <= 0) {
            this.nightlightsRef.push(newNightlight);
          } else {
            newNightlight = values[0];
          }

          if (discovered.length <= 0) {
            this.nightlights.push(newNightlight);
          }
        });
      } else if (discovered.length > 0) {
        this.nightlights.splice(this.nightlights.indexOf(discovered[0]), 1);
      }
    });
  }

  getByName(name: string): AngularFireList<Nightlight> {
    return this.afDB.list<Nightlight>(`users/${this.authService.user.uid}/nightlights`, ref => ref.orderByChild('name').equalTo(name))
  }

  rename(nightlight: Nightlight) {
    let alert = this.alertCtrl.create({
      title: 'Rename nightlight', message: 'Please enter the new name of your nightlight', cssClass: 'alert-buttons',
      inputs: [{
        name: 'customName',
        placeholder: 'Nightlight name',
        type: 'text'
      }],
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
      }, {
        text: 'Rename',
        cssClass: 'button-add',
        handler: (data) => {
          nightlight.customName = data.customName;
          this.getByName(nightlight.name).update(nightlight.key, nightlight);
        }
      }]
    });

    alert.present();
  }

  setDraw(nightlight: Nightlight, draw: Draw) {
    nightlight.draw = draw;
    this.mqttService.unsafePublish('nightliot/' + nightlight.name, nightlight.draw.value);
    this.save(nightlight);
  }

  save(nightlight: Nightlight) {
    this.nightlightsRef.update(nightlight.key, nightlight);
  }
}
