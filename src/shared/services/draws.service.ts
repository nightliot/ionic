import {Injectable} from "@angular/core";
import {AngularFireDatabase} from "angularfire2/database";
import {Collection} from "../models/collection";
import {AuthService} from "./auth.service";
import {AlertController, App} from "ionic-angular";
import {Draw} from "../models/draw";
import {NavControllerBase} from "ionic-angular/navigation/nav-controller-base";
import {NightlightsService} from "./nightlights.service";
import {CollectionsService} from "./collections.service";

@Injectable()
export class DrawsService {
  private navCtrl: NavControllerBase;

  constructor(private app: App, private nightlightsService: NightlightsService, private collectionsService: CollectionsService, private afDB: AngularFireDatabase, private authService: AuthService, private alertCtrl: AlertController) {
    this.navCtrl = this.app.getActiveNav();
  }

  getDrawsByCollectionKey(key: string) {
    return this.afDB.object(`users/${this.authService.user.uid}/collections/${key}/draws`);
  }

  deleteSelectedDraws(collection: Collection, draws: Draw[], callback: () => void) {
    let alert = this.alertCtrl.create({
      title: 'Delete draws', message: 'Do you want to delete ' + (draws.length) + ' draw(s)?', cssClass: 'alert-buttons',
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
      }, {
        text: 'Delete',
        cssClass: 'button-delete',
        handler: () => {
          collection.draws = collection.draws.filter(draw => !draws.find(a => draw == a));
          this.saveDraws(collection);
          if (callback) {
            callback();
          }
        }
      }]
    });

    alert.present();
  }

  addDrawToCollectionAndDisplay(collection: Collection, draw: Draw) {
    if (!collection.draws) {
      collection.draws = [];
    }

    collection.draws.push(draw);
    this.saveDraws(collection);

    let radios = [];
    this.nightlightsService.nightlights.forEach(nightlight => radios.push({type: 'radio', label: nightlight.customName, value: nightlight}));

    let alert = this.alertCtrl.create({
      title: 'Add draw to ' + collection.name, message: 'Would you like to display this new draw on a nightlight ?', cssClass: 'alert-buttons',
      inputs: radios,
      buttons: [{
        text: 'No',
        cssClass: 'button-delete',
        handler: () => {
          this.navCtrl.parent.select(1);
        }
      }, {
        text: 'Yes',
        cssClass: 'button-add',
        handler: data => {
          this.navCtrl.parent.select(0);
        }
      }]
    });

    alert.present();
  }

  saveDraws(collection: Collection) {
    this.getDrawsByCollectionKey(collection.key).set(collection.draws).catch(error => console.log(error));
  }

  addDrawToCollection(draw: Draw) {
    let nameAlert = this.alertCtrl.create({
      title: 'Choose a collection to where to ave your new draw', cssClass: 'alert-buttons',
      inputs: [{
        name: "name",
        placeholder: "Draw name",
        type: "text"
      }],
      buttons: [{
        text: 'Cancel',
        cssClass: 'button-delete',
        role: 'cancel'
      }, {
        text: 'Save',
        cssClass: 'button-add',
        handler: (data) => {
          draw.name = data.name;

          let radios = [];
          this.collectionsService.customs.forEach(collection => radios.push({type: 'radio', label: collection.name, value: collection}));

          let alert = this.alertCtrl.create({
            title: 'Choose a collection to where to ave your new draw', cssClass: 'alert-buttons',
            inputs: radios,
            buttons: [{
              text: 'Cancel',
              cssClass: 'button-delete',
              role: 'cancel'
            }, {
              text: 'Save',
              cssClass: 'button-add',
              handler: (collection) => {
                if (!collection.draws || collection.draws == {}) {
                  collection.draws = [];
                }

                collection.draws.push(draw);
                this.saveDraws(collection);
              }
            }]
          });

          alert.present();
        }
      }]
    });

    nameAlert.present();
  }

  delete(collection: Collection, draw: Draw, callback?: () => void) {
    let alert = this.alertCtrl.create({
      title: 'Delete draw', message: 'Do you want to delete ' + draw.name + ' ?', cssClass: 'alert-buttons',
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
      }, {
        text: 'Delete',
        cssClass: 'button-delete',
        handler: () => {
          collection.draws = collection.draws.filter(item => { return item != draw });
          this.saveDraws(collection);
          if (callback) {
            callback();
          }
        }
      }]
    });

    alert.present();
  }

  rename(collection: Collection, draw: Draw, callback?: () => void) {
    let alert = this.alertCtrl.create({
      title: 'Rename draw', message: 'Enter the new name for your draw.', cssClass: 'alert-buttons',
      inputs: [{
        name: 'name',
        placeholder: 'Draw name',
        type: 'text'
      }],
      buttons: [{
        text: 'Cancel',
        role: 'cancel'
      }, {
        text: 'Rename',
        cssClass: 'button-add',
        handler: data => {
          collection.draws.forEach(item => { if (item == draw) item.name = data.name });
          this.saveDraws(collection);
          if (callback) {
            callback();
          }
        }
      }]
    });

    alert.present();
  }

  display(draw: Draw) {
    let radios = [];
    this.nightlightsService.nightlights.forEach(nightlight => radios.push({type: 'radio', label: nightlight.customName, value: nightlight}));

    let alert = this.alertCtrl.create({
      title: 'Display draw', message: 'Choose a nightlight on which you want to display this draw', cssClass: 'alert-buttons',
      inputs: radios,
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'button-delete',
      }, {
        text: 'Display',
        cssClass: 'button-add',
        handler: nightlight => {
          this.nightlightsService.setDraw(nightlight, draw);
          this.navCtrl.parent.select(0);
        }
      }]
    });

    alert.present();
  }
}
