import {Draw} from "./draw";

export interface Nightlight {
  key?: string;
  domain: string;
  type: string;
  name: string;
  customName?: string;
  port: number;
  hostname: string;
  ipv4Addresses: string[];
  ipv6Addresses: string[];
  txtRecord: object;
  draw?: Draw;
}
