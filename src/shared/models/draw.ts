export class Draw {
  public name: string;
  public value: string;
  public thumbnail: string;
  public date: number = Date.now();
  public selected? : boolean = false;
}
