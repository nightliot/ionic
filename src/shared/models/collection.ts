import {Draw} from "./draw";

export class Collection {
  public key?: string;
  public name: string;
  public draws: Draw[] = [];
  public date: number = Date.now();
  public selected? : boolean = false;
}
