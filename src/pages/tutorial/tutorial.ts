import { Component } from '@angular/core';
import {NavController} from "ionic-angular";
import {TabsPage} from "../tabs/tabs";
import {Storage} from "@ionic/storage";
import {LoginPage} from "../login/login";
import {AngularFireAuth} from "angularfire2/auth";
import {AuthService} from "../../shared/services/auth.service";

@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html'
})
export class TutorialPage {
  slides = [{
    title: "Welcome to Nightliot!",
    description: "This tutorial will help you connect to your nightlights and display your draws according to your imagination.",
  }, {
    title: "Configure your Nightliot",
    description: "Connect to your new nightliot access point and follow the instructions in order to configure its WiFi network.",
    image: "assets/imgs/ica-slidebox-img-3.png",
  }, {
    title: "Configure your phone",
    description: "Put your phone on the same WiFi network as your nightliot.",
    image: "assets/imgs/ica-slidebox-img-2.png",
  }, {
    title: "Network scanning",
    description: "If you did well, the application will find all the nightliots connected to the same WiFi network as your phone.",
    image: "assets/imgs/ica-slidebox-img-1.png",
  }];

  constructor(private navCtrl: NavController, private storage: Storage, private afAuth: AngularFireAuth, private authService: AuthService) {}

  finishTutorial() {
    this.storage.set("tutorial", true).then(() => {
      this.afAuth.authState.subscribe(user => {
        this.authService.user = user;

        if (user) {
          this.navCtrl.setRoot(TabsPage);
        } else {
          this.navCtrl.setRoot(LoginPage);
        }
      });
    });
  }
}
