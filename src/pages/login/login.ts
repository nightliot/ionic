import {Component} from '@angular/core';
import {AuthService} from "../../shared/services/auth.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  private loginForm: FormGroup;
  private error : string = null;

  constructor(private authService: AuthService, private formBuilder: FormBuilder) {
    this.loginForm = this.formBuilder.group({
      email: ['konrad.kevin78@gmail.com', Validators.required],
      password: ['PouetTest', Validators.required],
    });
  }

  login() {
    this.authService.login(this.loginForm.value.email, this.loginForm.value.password, (error) => this.error = error);
  }

  register() {
    this.authService.register(this.loginForm.value.email, this.loginForm.value.password, (error) => this.error = error);
  }
}
