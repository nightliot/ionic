import {Component} from '@angular/core';
import {NavController, PopoverController} from 'ionic-angular';
import {CollectionDetailsPage} from "../collection-details/collection-details";
import {CollectionsPopoverPage} from "./collections-popover";
import {CollectionsService} from "../../shared/services/collections.service";
import {Collection} from "../../shared/models/collection";

@Component({
  selector: 'page-collections',
  templateUrl: 'collections.html'
})
export class CollectionsPage {
  public order: string = 'name';
  public selected: Collection[] = [];
  private customCollections: Collection[];
  private defaultCollections: Collection[];

  constructor(public navCtrl: NavController, private popoverCtrl: PopoverController, private collectionsService: CollectionsService) {}

  ionViewWillEnter() {
    this.getDefaultCollections();
    this.getCustomCollections();
  }

  getDefaultCollections() {
    this.collectionsService.defaultCollectionsRef.valueChanges().subscribe(collections => {
      this.defaultCollections = collections;
      this.defaultCollections.sort((a, b) => {return (a[this.order] > b[this.order]) ? 1 : ((b[this.order] > a[this.order]) ? -1 : 0)} );
    });
  }

  getCustomCollections() {
    this.collectionsService.customCollectionsRef.snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    }).subscribe(collections => {
      this.customCollections = collections;
      this.customCollections.sort((a, b) => {return (a[this.order] > b[this.order]) ? 1 : ((b[this.order] > a[this.order]) ? -1 : 0)} );
    });
  }

  selectCollection(collection) {
    this.selected.push(collection);
  }

  goToCollection(collection, canBeSelected: boolean) {
    if (this.selected.length > 0 && canBeSelected) {
      let index = this.selected.indexOf(collection);

      if (index >= 0) {
        this.selected.splice(index, 1);
      } else {
        this.selected.push(collection);
      }
    } else {
      this.navCtrl.push(CollectionDetailsPage, collection);
    }
  }

  deleteSelectedCollections() {
    this.collectionsService.deleteSelectedCollections(this.selected, () => this.selected = []);
  }

  addCollection() {
    this.collectionsService.addCollection();
  }

  showReorderMenu(event) {
    let popover = this.popoverCtrl.create(CollectionsPopoverPage, {order: this.order});
    popover.present({ev: event});
    popover.onDidDismiss(order => {
      this.order = order ? order : this.order;
      this.getCustomCollections();
    });
  }
}
