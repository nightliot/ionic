import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {Nightlight} from "../../shared/models/nightlight";
import {NightlightPage} from "../nightlight/nightlight";
import {NightlightsService} from "../../shared/services/nightlights.service";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private nightlights: Nightlight[];

  constructor(public navCtrl: NavController, public nightlightsService: NightlightsService) {
    this.nightlights = this.nightlightsService.nightlights;
  }

  ionViewWillEnter() {
    this.nightlightsService.watchNetwork();
  }

  ionViewWillLeave() {
    this.nightlightsService.unwatchNetwork();
  }

  goToNightlight(nightlight: Nightlight) {
    this.navCtrl.push(NightlightPage, nightlight);
  }
}
