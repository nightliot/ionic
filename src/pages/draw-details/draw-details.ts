import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {Draw} from "../../shared/models/draw";
import {Collection} from "../../shared/models/collection";
import {DrawsService} from "../../shared/services/draws.service";

@Component({
  selector: 'page-draw-details',
  templateUrl: 'draw-details.html'
})
export class DrawDetailsPage {
  private collection: Collection;
  private draw: Draw;
  private matrixSize = 8;
  private matrix: string[][] = new Array<string[]>(this.matrixSize);

  constructor(public navCtrl: NavController, private navParams: NavParams, private drawsService: DrawsService) {
    this.collection = this.navParams.data.collection;
    this.draw = this.navParams.data.draw;
    this.initMatrix();
  }

  initMatrix() {
    for (let row: number = 0; row < this.matrix.length; row++) {
      this.matrix[row] = new Array<string>(this.matrixSize);
      for (let col: number = 0; col < this.matrix[row].length; col++) {
        this.matrix[row][col] = this.draw ? this.draw.value[row * this.matrix.length + col] : '0';
      }
    }
  }

  display() {
    this.drawsService.display(this.draw);
  }

  rename() {
    this.drawsService.rename(this.collection, this.draw);
  }

  delete() {
    this.drawsService.delete(this.collection, this.draw, () => this.navCtrl.pop());
  }
}
