import {Component} from '@angular/core';
import {HomePage} from '../home/home';
import {CollectionsPage} from '../collections/collections';
import {NavController} from "ionic-angular";
import {AuthService} from "../../shared/services/auth.service";
import {LoginPage} from "../login/login";

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab1Root = HomePage;
  tab2Root = CollectionsPage;

  constructor(public navCtrl: NavController, private authService: AuthService) {}

   signOut() {
    this.authService.signOut();
    this.navCtrl.setRoot(LoginPage);
    this.navCtrl.popToRoot();
    window.location.reload();
  }
}
