import {Component} from '@angular/core';
import {AlertController, NavController, NavParams} from 'ionic-angular';
import {Collection} from "../../shared/models/collection";
import {Draw} from "../../shared/models/draw";
import {DrawsService} from "../../shared/services/draws.service";
import {CollectionsService} from "../../shared/services/collections.service";
import {NightlightsService} from "../../shared/services/nightlights.service";

@Component({
  selector: 'page-draw-new',
  templateUrl: 'draw-new.html'
})
export class DrawNewPage {
  private collection: Collection;
  private collections: Collection[];
  private draw: Draw;
  private matrixSize = 8;
  private matrix: string[][] = new Array<string[]>(this.matrixSize);

  constructor(public navCtrl: NavController, private navParams: NavParams, private drawsService: DrawsService, private nightlightsService: NightlightsService, private alertCtrl: AlertController, private collectionsService: CollectionsService) {
    this.collectionsService.customCollectionsRef.snapshotChanges().map(changes => {
      return changes.map(c => ({ key: c.payload.key, ...c.payload.val() }));
    }).subscribe(collections => {
      this.collections = collections;

      if (this.navParams.data) {
        this.collection = this.collections.filter((value) => {return value.key == this.navParams.data.key})[0];
      }

      if (!this.collection || (!this.collection.key && this.collections.length > 0)) {
        this.collection = this.collections[0];
      }
    });

    this.initMatrix('0');
    this.draw = { name: 'New draw', date: Date.now(), value: this.matrixToString(), thumbnail: '' };
  }

  initMatrix(value: string) {
    for(let i: number = 0; i < this.matrix.length; i++) {
      this.matrix[i] = new Array<string>(this.matrixSize);
      for(let j: number = 0; j < this.matrix[i].length; j++) {
        this.matrix[i][j] = value;
      }
    }
  }

  updateMatrix(row: number, col: number) {
    this.matrix[row][col] = this.matrix[row][col] == '0' ? '1' : '0';
    this.draw.value = this.matrixToString();
  }

  matrixToString() {
    let message : string = "";
    for (let i = 0; i < this.matrix.length; i++) {
      message += this.matrix[i].join("");
    }

    return message;
  }

  addDrawToCollection() {
    if (!this.collection.draws) {
      this.collection.draws = [];
    }

    this.collection.draws.push(this.draw);
    this.drawsService.saveDraws(this.collection);

    let radios = [];
    this.nightlightsService.nightlights.forEach(nightlight => radios.push({type: 'radio', label: nightlight.customName, value: nightlight}));

    let alert = this.alertCtrl.create({
      title: 'Add draw to ' + this.collection.name, message: 'Would you like to display this new draw on a nightlight ?', cssClass: 'alert-buttons',
      inputs: radios,
      buttons: [{
        text: 'No',
        cssClass: 'button-delete',
        handler: () => {
          this.navCtrl.pop().then(() => this.navCtrl.parent.select(1));
        }
      }, {
        text: 'Yes',
        cssClass: 'button-add',
        handler: (nightlight) => {
          this.nightlightsService.setDraw(nightlight, this.draw);
          this.navCtrl.pop();
          this.navCtrl.pop();
          this.navCtrl.parent.select(0);
        }
      }]
    });

    alert.present();
  }
}
