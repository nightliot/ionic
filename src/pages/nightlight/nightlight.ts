import {Component} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {Nightlight} from "../../shared/models/nightlight";
import {NightlightsService} from "../../shared/services/nightlights.service";
import {DrawsService} from "../../shared/services/draws.service";

@Component({
  selector: 'page-nightlight',
  templateUrl: 'nightlight.html'
})
export class NightlightPage {
  private nightlight: Nightlight;
  private matrixSize = 8;
  private matrix: string[][] = new Array<string[]>(this.matrixSize);

  constructor(public navCtrl: NavController, private navParams: NavParams, private nightlightService: NightlightsService, private drawsService: DrawsService) {
    this.nightlight = this.navParams.data;
    this.initMatrix();
    this.setDraw();
  }

  initMatrix() {
    for (let row: number = 0; row < this.matrix.length; row++) {
      this.matrix[row] = new Array<string>(this.matrixSize);
      for (let col: number = 0; col < this.matrix[row].length; col++) {
        this.matrix[row][col] = this.nightlight.draw ? this.nightlight.draw.value[row * this.matrix.length + col] : '0';
      }
    }
  }

  updateMatrix(row: number, col: number) {
    this.matrix[row][col] = this.matrix[row][col] == '0' ? '1' : '0';
    this.nightlight.draw.value = this.matrixToString();
    this.setDraw();
  }

  matrixToString() {
    let message : string = "";
    for (let i = 0; i < this.matrix.length; i++) {
      message += this.matrix[i].join("");
    }

    return message;
  }

  setDraw() {
    if (!this.nightlight.draw) {
      this.nightlight.draw = { name: "Default", value:  this.matrixToString(), thumbnail: "", date: Date.now()};
    }

    this.nightlightService.setDraw(this.nightlight, this.nightlight.draw);
  }

  saveDraw() {
    this.drawsService.addDrawToCollection(this.nightlight.draw);
  }

  rename(nightlight: Nightlight) {
    this.nightlightService.rename(nightlight);
  }

  showCollections() {
    this.navCtrl.parent.select(1);
  }
}
