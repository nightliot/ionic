import {NavParams, ViewController} from "ionic-angular";
import {Component} from "@angular/core";

@Component({
  template: `
    <ion-list radio-group [(ngModel)]="order" (ionChange)="close()">
      <ion-list-header>Order collections by</ion-list-header>
      
      <ion-item>
        <ion-label>Name</ion-label>
        <ion-radio value="name" onclick="close()"></ion-radio>
      </ion-item>

      <ion-item>
        <ion-label>Creation date</ion-label>
        <ion-radio value="-date" onclick="close()"></ion-radio>
      </ion-item>
      
    </ion-list>
  `
})
export class CollectionDetailsPopoverPage {
  private order: string;
  private previous: string;

  constructor(public viewCtrl: ViewController, private navParams: NavParams) {
    this.previous = this.navParams.data.order;
    this.order = this.navParams.data.order;
  }

  close() {
    if (this.previous != this.order) {
      this.viewCtrl.dismiss(this.order);
    }
  }
}
