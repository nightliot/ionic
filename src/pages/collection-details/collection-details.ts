import {Component} from '@angular/core';
import {NavController, NavParams, PopoverController} from 'ionic-angular';
import {Draw} from "../../shared/models/draw";
import {CollectionDetailsPopoverPage} from "./collection-details-popover";
import {DrawDetailsPage} from "../draw-details/draw-details";
import {DrawsService} from "../../shared/services/draws.service";
import {DrawNewPage} from "../draw-new/draw-new";
import {CollectionsService} from "../../shared/services/collections.service";

@Component({
  selector: 'page-collection-details',
  templateUrl: 'collection-details.html'
})
export class CollectionDetailsPage {
  public order: string = 'name';
  private collection: any;
  private selected: Draw[] = [];

  constructor(public navCtrl: NavController, private collectionsService: CollectionsService, private drawsService: DrawsService, private navParams: NavParams, private popoverCtrl: PopoverController) {
    this.collection = this.navParams.data;
  }

  selectDraw(draw: Draw) {
    this.selected.push(draw);
  }

  goToDraw(draw: Draw) {
    if (this.selected.length > 0) {
      let index = this.selected.indexOf(draw);

      if (index >= 0) {
        this.selected.splice(index, 1);
      } else {
        this.selected.push(draw);
      }
    } else {
      this.navCtrl.push(DrawDetailsPage, {collection: this.collection, draw: draw});
    }
  }

  deleteSelectedDraws() {
    this.drawsService.deleteSelectedDraws(this.collection, this.selected, () => this.selected = []);
  }

  showReorderMenu(event) {
    let popover = this.popoverCtrl.create(CollectionDetailsPopoverPage, {order: this.order});
    popover.present({ev: event});
    popover.onDidDismiss(order => this.order = order ? order : this.order);
  }

  showNewDraw() {
    this.navCtrl.push(DrawNewPage, this.collection);
  }

  rename() {
    this.collectionsService.rename(this.collection);
  }

  delete() {
    this.collectionsService.delete(this.collection, () => this.navCtrl.pop());
  }
}
